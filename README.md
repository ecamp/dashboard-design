# eCamp Dashboard design

This is the repository of the eCamp Dashboard design. There is no code here, just design. 

## UX Project Checklist

To follow the UX design progress you can view the checklist [here](UX-PROJECT-CHECKLIST.md).

## Sitemap

```
|--_ architecture :: information architecture of page
|  |-- template.xml
|  |-- ideas.md
|--_ assets :: logos, colors, etc.
|  |--_ logos
|  |--_ favicon
|  |-- color-scheme.md
|--_ masterfiles :: vector files (.figm, .ai, .psd, etc.)
|--_ mocks :: iterations/exports
|  |--_ deprecated
|  |--_ v1
|  |--_ v2
|  ...
|--_ screenshots :: screenshots of production (changes frequently)
   |--_ before :: previous version
   |--_ after :: current version
```
