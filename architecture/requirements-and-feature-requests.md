# Requirements and feature requests of different parties

Getting an overview about the requirements of all target groups is the focus first on camp planning and later course planning. Course planning needs to be more formal and has stricter guidelines.

Generally, most of the functionality in eCamp v2 should be available in eCamp v3 as well. For some select obscure features, usage statistics will be gathered on the running system, and depending on the outcome, they might not be included in the new version (such as notes on a camp day, digital contact card download, task list ("Aufgabenliste"), etc.). In any case, the first step in redevelopment is to re-create the core functionality of the existing system. Once this minimal viable product (MVP) has been completed, more functionality can be added. In the list below, we try to keep track of some of the requested features that might be implemented in the future.

The target groups and stakeholders of the project are as follows:

- Camp and course main leaders
- Additional leaders
- Group leaders (Abteilungsleiter)
- J+S-Coaches and LKB
- PBS personnel

Other organizations and groups relevant to the project are:

- PBS: KA AuB, Akom, Bkom
- Project MiData / hitobito
- Project new NDBJS (WTO still running at BASPO as of July 2018)
- Developers of similar systems, such as my-scout-camp.ch and leiterkurse.ch

These organizations should be involved and communicated with at various stages of the project.

## PBS - Project DigiScout
- **eCamp Languages:** support (at least) German, French, Italian
- **Course planning:** optimizing course planning functionality
- **Connect camp planning and automatize camp administration:** extend eCamp for connection to camp planning and camp administration papers and automation (i.e. option to automatically send forms to PBS and cantonal secretariat).
- **Connect course planning and automatize course administration:** forms for courses can automatically be sent to PBS and cantonal secretariat
- collecting data from planning to create statistics which will help to set focus points in PBS and cantonal associations. This data collection should not increase the workload for the users.
- **Links to PBS camp and course planning documentation:** In appropriate locations within the web app, links to relevant informational documents are embedded. Or alternatively, create a kind of wizard guiding users through planning their camps.
- **Administration by PBS:** The responsible KA-leaders from PBS should be able to change some data (such as required course education goals or links to the PBS documentation) without requiring code changes
- **Create documentation:** For users and for federal level users, documentation should exist on how to use the application.
- **Integration with related systems:** MiData, NDBJS / SPORTdb / new NDBJS (under construction), PBS platform for digital tools (under construction), PBS personnel planning tool (part of DigiScout2020), swisstopo / map.geo.admin.ch / map.wanderland.ch

## User camp planning tool

- has to be responsive. It is often opened on mobile or smaller devices.
- Has to be available for offline use. Creating a PWA should be fairly easy with a modern JS framework in the frontend and a sensible API in the backend.
- give us the option to open several sessions or a function to compare directly with old camps (copy-paste makes life easier)
- short-cuts/short-keys to speed up the usage (low priority, because only advanced user would use it)
- integration of forms from third parties - simplify and centralize process
- open a camp in MiData automatically opens a camp in eCamp
- the provided map is completely useless (in case it should work for once)
- add additional (third party) PDFs to complete PDF (already existent in eCamp v2, see PDF drucken -> PDF)
- Ability to see number of participants on the camp while planning a block (information pulled from MiData)
- Usable for other Swiss youth organizations, such as JuBla and Cevi. Also contact their rerpresentants and gather their requirements.
- Making eCamp optional for certain steps of the planning process, such that e.g. users that wish to create a hand-drawn Grobprogramm can do so and aren't forced by PBS regulation to use eCamp for this part of camp planning.
- Open question: Will the migration of data from eCamp v2 in some form be possible?

## User course planning tool

- course planning has more demands than camp planning (concrete list coming soon)
    *
- integration of forms from third parties - simplify and centralize the course planning process

## Coaches/Supervisor

- simplify supervision of camps/courses to decrease load of work
- uniform documents in all steps would be great
- coach should be able to change the completion percentage field on blocks

## Technical requirements
- **Modularity:** The software should at least be separated into backend and frontend, with a sensible API in between to allow for replacement of either part.
- **Extensibility:** The software should be extensible in the sense that new developers can contribute to the project easily and propose fixes. This is necessary in case some contributers leave the project.
- **Automatically tested:** Automated unit tests and end-to-end tests should be created to check the integrity of the code before every merge and deployment.
- **Operation:** PBS have stated their intentions to provide aid in operating the software. This may include financing, support and providing a suitable subdomain such as ecamp.scout.ch